<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clefs secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C'est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d'installation. Vous n'avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'newsletter_interne');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', '');

/** Adresse de l'hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N'y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clefs uniques d'authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n'importe quel moment, afin d'invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'u~HQd??`Ri#+b$s+s13Wf9{&R1mE|c)i@|^oJ).Q+|cy`FvMck.Kc>h#){+db}2I');
define('SECURE_AUTH_KEY',  'BA KmN,-x.Nc9tJDPB8kS-=GL0Of%bstwxwn >VI_-O,ULIiwwW1Oy{:C vU]-),');
define('LOGGED_IN_KEY',    'a![u1F>&H;{+1kKndFIJfakrn`)Wk-~h{NtLhWb2#-2^;5ZMtQjlS,%MK1|f!<;9');
define('NONCE_KEY',        'k|JK4p||dh-9>K<IbcU2`bMj}+Z78(~A_M|<Ig01[6OQd?]NABW1|^,(|$y@2h,r');
define('AUTH_SALT',        '^y,{>%CW[j[Uf>m)Y;W0DX`;uG[v#*uAA(J&Yon4vydUYSkv|08HUDyxh!OBvY8-');
define('SECURE_AUTH_SALT', 'A@S|!&,wPqJTC=C!P[u7:$72dzQy/@)zsDy>xp2--W?IcrW9[cgWO>mMC+13*7tn');
define('LOGGED_IN_SALT',   '.D2I n^x]~Qj2` AwphfnG++=8|*7hBtF$ZBf7gbt=*EwLk>,AGXhV#?~+Z8>U_i');
define('NONCE_SALT',       '?RSp^g~ ,+N+?>6Id&oyt1)WASG<] ;l_nqZlk[<?[)yJOwv?C+$Dr-T{^5P>Xsh');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N'utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés!
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l'affichage des
 * notifications d'erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d'extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 */
define('WP_DEBUG', false);

/* C'est tout, ne touchez pas à ce qui suit ! Bon blogging ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');